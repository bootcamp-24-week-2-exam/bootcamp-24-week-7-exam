package com.example.StudentInformationSystemWithSpring.controller.services;

import com.example.StudentInformationSystemWithSpring.model.Sections;
import com.example.StudentInformationSystemWithSpring.model.Student;
import com.example.StudentInformationSystemWithSpring.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionService {
    @Autowired
    SectionRepository sectionRepository;
    public List<Sections> getAll(){
        return sectionRepository.findAll();
    }

    public Sections addSection(Sections sections){
        return sectionRepository.save(sections);
    }
}
