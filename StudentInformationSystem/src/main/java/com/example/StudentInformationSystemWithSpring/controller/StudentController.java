package com.example.StudentInformationSystemWithSpring.controller;


import com.example.StudentInformationSystemWithSpring.controller.services.StudentSearchService;
import com.example.StudentInformationSystemWithSpring.controller.services.StudentService;
import com.example.StudentInformationSystemWithSpring.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <H1>Student Information Controller</H1>
 * <p>This class use to Add, Edit, Delete and Search the Student Information</p>
 * <p>This is for Mapping to GetMapping, PostMapping, PutMapping and DeleteMapping</p>
 *
 * @author samuel.guban
 * @author paul.samson
 * @since 2022-08-22
 */
@RestController
@RequestMapping("/")
@CrossOrigin("http://localhost:3000")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    StudentSearchService studentSearchService;

    // Welcome home page.
    @GetMapping()
    @ResponseBody
    public String home(){
        return "Welcome!";
    }

    // show all.

    /**
     * This method is for adding student information
     * @param student data for add
     */
    @PostMapping("/add")
    public void addStudent(@RequestBody Student student) {
        studentService.AddStudent(student);
    }

    /**
     * This method is for updating student information
     * using student id
     * @param student data for update
     * @param id of student
     */
    @PutMapping("/edit/{id}")
    public void editStudent(@RequestBody Student student, @PathVariable Integer id) {
        studentService.EditStudent(student, id);
    }

    /**
     * This method is for deleting student
     * using student id
     * @param id of student
     */
    @DeleteMapping("/delete/{id}")
    public void deleteStudent(@PathVariable Integer id) {
        System.out.println(id);
        studentService.DeleteStudent(id);
    }

    /**
     * This method use to get the list of students and sorted by lastname
     * @return List of student sorted by lastname
     */
    //http://localhost:8080/search/sort
    @GetMapping("/search/sort")
    public List<Student> search(){
        return studentSearchService.getStudSortByNameYrLvlSec();
    }

    /**
     * This method use to get the list of students
     * by searching the year level and sorted by lastname
     * @param yrlvl year level
     * @return List of student filtered by year level and sorted by lastname
     */
    //http://localhost:8080/search/byYrLvl?yrlvl=3
    @GetMapping("/search/byYrLvl")
    public List<Student> searchByYrLvl(@RequestParam Integer yrlvl){
        return studentSearchService.getStudPerYrLvl(yrlvl);
    }

    /**
     * This method use to get the list of students
     * by searching the section id and sorted by lastname
     * @param section section id
     * @return List of student filtered by section and sorted by lastname
     */
    //http://localhost:8080/search/bySec?section=1
    @GetMapping("/search/bySec")
    public List<Student> searchBySec(@RequestParam Integer section){
        return studentSearchService.getStudPerSec(section);
    }

    /**
     * This method use to get the list of students
     * by searching the city and sorted by lastname
     * @param city
     * @return List of student filtered by city and sorted by lastname
     */
    //http://localhost:8080/search/byCity?city=Zamboanga city
    @GetMapping("/search/byCity")
    public List<Student> searchByCity(@RequestParam String city){
        return studentSearchService.getStudPerCity(city);
    }

    /**
     * This method use to get the list of students
     * by searching the zip and sorted by lastname
     * @param zip
     * @return List of student filtered by zip and sorted by lastname
     */
    //http://localhost:8080/search/byZip?zip=7000
    @GetMapping("/search/byZip")
    public List<Student> searchByZip(@RequestParam Integer zip){
        return studentSearchService.getStudPerZip(zip);
    }

//    @GetMapping("/search/byAge")
//    public List<Student> searchByAll(@RequestParam List<Integer> range){
//        return studentSearchService.getStudPerAgeBracket(range.get(0), range.get(1));
//    }

    /**
     * This method use to get the list of students
     * @return List of student
     */
    @GetMapping("/search/byAll")
    public List<Student> searchByAll(){
//        return studentSearchService.getAll();
        return studentSearchService.getStudents();
    }

    /**
     * This method use to get the list of students
     * by searching the age between minimum and maximum age
     * then sorted by lastname
     * @param minimum age
     * @param maximum age
     * @return List of student between minimum and maximum age then sorted by lastname
     */
    //http://localhost:8080/search/byAge?minimum=15&maximum=18
    @GetMapping("/search/byAge")
    public List<Student> searchByAge(@RequestParam Integer minimum, @RequestParam Integer maximum){
        return studentSearchService.getStudPerAgeBracket(minimum, maximum);
    }

    /**
     * This method use to get the student
     * by searching the Lastname, Firstname and Id
     * @param search - Lastname, Firstname and Id
     * @return student
     */
    @GetMapping("/search/{search}")
    public List<Student> searchByLastnameFirstnameId(@PathVariable String search){
        int id = 0;
        try{
            id = Integer.parseInt(search);
        }catch (Exception e){
            id = 0;
        }
        return studentSearchService.getByLastnameFirstnameMiddlenameId("%"+search+"%", "%"+search+"%", "%"+search+"%", id);
    }
}
