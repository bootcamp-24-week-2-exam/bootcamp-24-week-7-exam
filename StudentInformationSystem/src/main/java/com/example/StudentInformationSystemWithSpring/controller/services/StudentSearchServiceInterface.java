package com.example.StudentInformationSystemWithSpring.controller.services;

import com.example.StudentInformationSystemWithSpring.model.Student;

import java.util.List;

public interface StudentSearchServiceInterface {

    List<Student> getStudSortByNameYrLvlSec();
    List<Student> getStudPerYrLvl(int ylvl);
//    List<Student> getStudPerSec(String section);
    List<Student> getStudPerSec(int sectionId);
    List<Student> getStudPerCity(String city);
    List<Student> getStudPerZip(int zip);
    List<Student> getStudPerAgeBracket(int min, int max);
    List<Student> getAll();
    List<Student> getByLastnameFirstnameMiddlenameId(String Lastname, String Firstname, String Middlename, int Id);

    List<Student> getStudents();
}
