package com.example.StudentInformationSystemWithSpring.model;


import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

/**
 *  <H1>Student Information</H1>
 * <p>This class use to set ang get the Student Information</p>
 *
 * @author samuel.guban
 * @author lawrence.chan
 * @since 2022-08-22
 */
@Entity (name = "tbl_students")
@Data
//@Builder
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "stud_id", length = 11)
    private Integer id;

    @Column (name = "stud_firstname", nullable = false)
    private String firstname;

    @Column (name = "stud_lastname", nullable = false)
    private String lastname;

    @Column (name = "stud_middlename", nullable = false)
    private String middlename;

    @Column (name = "stud_age", nullable = false, length = 10)
    private Integer age;

    @Column (name = "stud_address", nullable = false)
    private String address;

    @Column (name = "stud_zip", nullable = false, length = 10)
    private Integer zip;

    @Column (name = "stud_city", nullable = false)
    private String city;

    @Column (name = "stud_regionprovince", nullable = false)
    private String regionProvince;

    @Column (name = "stud_phoneno", nullable = false, unique = true)
    private String phoneNo;

    @Column (name = "stud_mobileno", nullable = false, unique = true)
    private String mobileNo;

    @Column (name = "stud_email", nullable = false, unique = true)
    private String email;

    @Column (name = "stud_yrlvl", nullable = false, length = 5)
    private Integer yrlvl;

    @Column (name = "stud_sectionid", nullable = false, length = 11)
    private Integer sectionId;

    @ManyToOne
    @JoinColumn(name = "stud_sectionid", referencedColumnName = "sec_id", insertable=false, updatable=false)
//    @Column (name = "sec_section", nullable = false, length = 11)
    private Sections section;

//    @ManyToOne
//    @JoinColumn(name = "stud_sectionid", insertable = false, updatable = false)
//    private Sections sections;

    /**
     * This is constructor - unused
     */
    public Student() {
    }

    /**
     * Construct a Student information with section id
     * @param id
     * @param firstname
     * @param lastname
     * @param middlename
     * @param age
     * @param address
     * @param zip
     * @param city
     * @param regionProvince
     * @param phoneNo
     * @param mobileNo
     * @param email
     * @param yrlvl
     * @param sectionId
     */
    public Student(Integer id, String firstname, String lastname, String middlename, Integer age, String address, Integer zip, String city, String regionProvince, String phoneNo, String mobileNo, String email, Integer yrlvl, Integer sectionId) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.age = age;
        this.address = address;
        this.zip = zip;
        this.city = city;
        this.regionProvince = regionProvince;
        this.phoneNo = phoneNo;
        this.mobileNo = mobileNo;
        this.email = email;
        this.yrlvl = yrlvl;
        this.sectionId = sectionId;
    }

    /**
     * Construct a Student information with Section object (section id and section name)
     * @param id
     * @param firstname
     * @param lastname
     * @param middlename
     * @param age
     * @param address
     * @param zip
     * @param city
     * @param regionProvince
     * @param phoneNo
     * @param mobileNo
     * @param email
     * @param yrlvl
     * @param section
     */
    public Student(Integer id, String firstname, String lastname, String middlename, Integer age, String address, Integer zip, String city, String regionProvince, String phoneNo, String mobileNo, String email, Integer yrlvl, Sections section) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.age = age;
        this.address = address;
        this.zip = zip;
        this.city = city;
        this.regionProvince = regionProvince;
        this.phoneNo = phoneNo;
        this.mobileNo = mobileNo;
        this.email = email;
        this.yrlvl = yrlvl;
        this.section = section;
    }

    /**
     * To get the Student ID
     * @return id - Student ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * To set the Student ID
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * To get the student firstname
     * @return firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * To set the student firstname
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * To get the student lastname
     * @return lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * To set the student lastname
     * @param lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * To get the student middlename
     * @return middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * To set student middlename
     * @param middlename
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * To get student age
     * @return age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * To set student age
     * @param age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * To get student address
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * To set student address
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * To get student zip
     * @return zip
     */
    public Integer getZip() {
        return zip;
    }

    /**
     * To set student zip
     * @param zip
     */
    public void setZip(Integer zip) {
        this.zip = zip;
    }

    /**
     * TO get student city
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * TO set student city
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * TO get student region province
     * @return regionProvince
     */
    public String getRegionProvince() {
        return regionProvince;
    }

    /**
     * To set student region province
     * @param regionProvince
     */
    public void setRegionProvince(String regionProvince) {
        this.regionProvince = regionProvince;
    }

    /**
     * To get student phone number
     * @return phoneNo
     */
    public String getPhoneNo() {
        return phoneNo;
    }

    /**
     * TO set student phone number
     * @param phoneNo
     */
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    /**
     * TO get student mobile number
     * @return mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * To set student mobile number
     * @param mobileNo
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * To get student email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * To set student email
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * to get student year level
     * @return yrlvl
     */
    public Integer getYrlvl() {
        return yrlvl;
    }

    /**
     * to set student year level
     * @param yrlvl
     */
    public void setYrlvl(Integer yrlvl) {
        this.yrlvl = yrlvl;
    }

    /**
     * To get student section id
     * @return sectionId
     */
    public Integer getSectionId() {
        return sectionId;
    }

    /**
     * To set student section id
     * @param sectionId
     */
    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    /**
     * To get student section
     * @return section and section id
     */
    public String getSections() {
        return section.getSection();
    }

    /**
     * To set student section
     * @param sections
     */
    public void setSections(String sections) {
        this.section.setSection(sections);
    }

}

