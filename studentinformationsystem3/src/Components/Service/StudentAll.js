import axios from "axios";
import { useState, useEffect } from "react";
import Posts from "../Posts";
import Pagination from "../Pagination";

function StudentAll({ api }) {
  const [getAllStudents, setgetAllStudents] = useState([]);

  //pagination start
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(5);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = getAllStudents.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  //pagination end
  console.log("currentPage: ", currentPage);
  // get all students.
  useEffect(() => {
    setLoading(true);
    axios
      .get("http://localhost:8080/search/" + api)
      .then((response) => {
        setgetAllStudents(response.data);
        // setting new student data with the new response.
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [api]);

  // get all students sort by name and year section.
  // useEffect(() => {
  //   axios
  //     .get("http://localhost:8080/search/sort", {
  //       // authenticate.
  //       auth: {
  //         username: "root",
  //         password: "root",
  //       },
  //     })
  //     .then((response) => {
  //       setStudentSortByNameYrSection(response.data);
  //       // console.log(response.data);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }, []);

  // // get all students by year level.
  // useEffect(() => {
  //   axios.get("http://localhost:8080/search/byYrLvl", {
  //     // authenticate.
  //     auth: {
  //       username: "root",
  //       password: "root"
  //     }
  //   }).then(response => {
  //     setStudentByYrLevel(response.data);
  //     console.log(response.data);
  //   }).catch(error => {
  //     console.log(error);
  //   })
  // }, [])

  // // get all students by Section.
  // useEffect(() => {
  //   axios.get("http://localhost:8080/search/bySec", {
  //     // authenticate.
  //     auth: {
  //       username: "root",
  //       password: "root"
  //     }
  //   }).then(response => {
  //     setStudentBySection(response.data);
  //     console.log(response.data);
  //   }).catch(error => {
  //     console.log(error);
  //   })
  // }, [])

  // // get all students by City.
  // useEffect(() => {
  //   axios.get("http://localhost:8080/search/byCity", {
  //     // authenticate.
  //     auth: {
  //       username: "root",
  //       password: "root"
  //     }
  //   }).then(response => {
  //     setStudentByCity(response.data);
  //     console.log(response.data);
  //   }).catch(error => {
  //     console.log(error);
  //   })
  // }, [])

  // // get all students by ZIP.
  // useEffect(() => {
  //   axios.get("http://localhost:8080/search/byZip", {
  //     // authenticate.
  //     auth: {
  //       username: "root",
  //       password: "root"
  //     }
  //   }).then(response => {
  //     setStudentByZip(response.data);
  //     console.log(response.data);
  //   }).catch(error => {
  //     console.log(error);
  //   })
  // }, [])

  // // get all students by Age.
  // useEffect(() => {
  //   axios.get("http://localhost:8080/search/byAge", {
  //     // authenticate.
  //     auth: {
  //       username: "root",
  //       password: "root"
  //     }
  //   }).then(response => {
  //     setStudentByAge(response.data);
  //     console.log(response.data);
  //   }).catch(error => {
  //     console.log(error);
  //   })
  // }, [])

  return (
    <div className="container mt-5">
      {/* <h1 className="text-primary mb-3">Student Information</h1> */}
      {/* <Posts posts={posts} loading={loading} /> */}
      <Posts
        posts={currentPosts}
        loading={loading}
        setgetAllStudents={setgetAllStudents}
        getAllStudents={getAllStudents}
      />
      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={getAllStudents.length}
        paginate={paginate}
      />
    </div>
  );
}

export default StudentAll;
